package redisprocess

/*
#cgo CFLAGS: -DRTI_UNIX -DRTI_LINUX -DRTI_64BIT -I/home/liguo/rti_connext_dds-5.3.1/include -I/home/liguo/rti_connext_dds-5.3.1/include/ndds
#cgo LDFLAGS: -L/home/liguo/RFM/Code/dataprocessing/lib -lDDSMessage -L/home/liguo/rti_connext_dds-5.3.1/lib/x64Linux3gcc5.4.0 -lnddscz -lnddscorez -lnddstransporttcp -ldl -lnsl -lm -lpthread -lrt
#include "../dds/DDS_RobotMessage.h"
#include "../dds/DDS_RobotMessageSupport.h"
#include "../dds/DDS_RobotMessagePlugin.h"
#include "../dds/DDS_RobotMessage_publisher.h"
#include "../dds/DDS_RobotMessage_subscriber.h"
*/
import "C"

import (
    "encoding/json"
    "fmt"
    "log"
    "os"
    "time"
    "unsafe"

    "../config"
    redis "github.com/go-redis/redis"
)

type Job struct {
    MsgID             string           `json:"msgID"`
    CompanyID         string           `json:"companyID"`
    RobotID           string           `json:"robotID"`
    RecurringJobID    string           `json:"recurringJobID"`
    JobID             string           `json:"jobID"`
    PriorityName      string           `json:"priority"`
    ScheduleType      string           `json:"scheduleType"`
    RecurringDays     int              `json:"recurringDays"`
    JobCreatedDate    time.Time        `json:"jobCreatedDate"`
    StartDate         time.Time        `json:"startDate"`
    EndDate           time.Time        `json:"endDate"`
    ExecutionTime     string           `json:"executionTime"`
    Abort             bool             `json:"abort"`
    TimeList          string           `json:"timeList"`
    Tasks             string           `json:"tasks"`
}

type AccessKey struct {
    CompanyID  string `json:"companyID"`
    RobotID    string `json:"robotID"`
    TimeStamp  uint   `json:"timeStamp"`
    AccessKey  string `json:"accessKey"`
    AdaptorKey string `json:"adaptorKey"`
}

type RobotJobResponse struct {
    CompanyID      string `json:"companyID"`
    RobotID        string `json:"robotID"`
    JobID          string `json:"jobID"`
    TimeStamp      uint   `json:"timeStamp"`
    RequestSuccess bool   `json:"requestSuccess"`
}

type NotificationResponse struct {
    CompanyID      string `json:"companyID"`
    RobotID        string `json:"robotID"`
    NotificationID string `json:"notificationID"`
    TimeStamp      uint   `json:"timeStamp"`
    Status         string `json:"status"`
    Details        string `json:"details"`
}

type JobRequestResponse struct {
    CompanyID      string `json:"companyID"`
    RobotID        string `json:"robotID"`
    TimeStamp      uint   `json:"timeStamp"`
    RequestSuccess bool   `json:"requestSuccess"`
}

type RobotMsgAck struct {
    MsgID            string `json:"msgID"`
    CompanyID        string `json:"companyID"`
    RobotID          string `json:"robotID"`
    TimeStamp        uint `json:"timeStamp"`
}

type TeleoperationRequest struct {
    MsgID               string  `json:"msgID"`
    CompanyID           string  `json:"companyID"`
    RobotID             string  `json:"robotID"`
    TeleopMode          bool    `json:"teleopMode"`
    TimeStamp           uint    `json:"timeStamp"`
}

type RobotControl struct {
    MsgID               string   `json:"msgID"`
    RobotID             string   `json:"robotID"`
    CompanyID           string   `json:"companyID"`
    TimeStamp           uint     `json:"timeStamp"`
    LinearVelocityX     float64  `json:"linearVelocityX"`
    LinearVelocityY     float64  `json:"linearVelocityY"`
    LinearVelocityZ     float64  `json:"linearVelocityZ"`
    AngularVelocityX    float64  `json:"angularVelocityX"`
    AngularVelocityY    float64  `json:"angularVelocityY"`
    AngularVelocityZ    float64  `json:"angularVelocityZ"`
}

type CameraControl struct {
    MsgID               string   `json:"msgID"`
    RobotID             string   `json:"robotID"`
    CompanyID           string   `json:"companyID"`
    TimeStamp           uint     `json:"timeStamp"`
    CameraName          string   `json:"cameraName"`
    Pan                 string   `json:"pan"`
    Tilt                string   `json:"tilt"`
    ZoomMultiplier      float64  `json:"zoomMultiplier"`
}

type ConferenceCallRequest struct {
    MsgID               string   `json:"msgID"`
    RobotID             string   `json:"robotID"`
    CompanyID           string   `json:"companyID"`
    TimeStamp           uint     `json:"timeStamp"`
    RequestID           string   `json:"requestID"`
    Mode                string   `json:"mode"`
    Room                string   `json:"room"`
    VideoOn             bool     `json:"videoOn"`
    AudioMuted          bool     `json:"audioMuted"`
}

type ConferenceCallUpdates struct {
    MsgID               string   `json:"msgID"`
    RobotID             string   `json:"robotID"`
    CompanyID           string   `json:"companyID"`
    TimeStamp           uint     `json:"timeStamp"`
    RequestID           string   `json:"requestID"`
    Room                string   `json:"room"`
    VideoOn             bool     `json:"videoOn"`
    AudioMuted          bool     `json:"audioMuted"`
    Status              string   `json:"status"`
}

var robotRedisClient *redis.Client

func RedisInit() {
    var err error

    robotRedisClient, err = config.Init()
    if err != nil {
        log.Println("redis init error:", err)
        fmt.Println("redis init error:", err)
    }

    // initialize the DDS publisher
    fmt.Println("start DDS publisher")

    useProfile := 0
    if (config.Get().UseProfile) == true {
        useProfile = 1
    }
    C.publisher_main(0, C.int(useProfile))
}

func SubRobotJobFromRedis() {

    // Subscribe for job channel
    redisChannelSub := robotRedisClient.Subscribe("JobChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received Job")
    } else {
        fmt.Println("redisChannelSub for Received Job is nil")
    }

    for {
        // subscribe new job from redis job channel
        job, _ := redisChannelSub.ReceiveMessage()

        // publish the new job to robot via DDS
        if (job != nil) && (job.Payload != "") {
            pubJobToDDS(job.Payload)
        }
    }
}

func pubJobToDDS(msg string) {

    // open the log file to log the job
    currentDate := time.Now()
    path := fmt.Sprintf("log/info-job-%s.log", currentDate.Format("2006-01-02"))
    f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("error opening file: %v", err)
    }
    defer f.Close()
    log.SetOutput(f)

    var job C.DDS_Job
    data := Job{}
    json.Unmarshal([]byte(msg), &data)

    // convert MQTT data format to DDS
    var tmpMsgID *C.char
    var tmpRobotID *C.char
    var tmpCompanyID *C.char
    var tmpRecurringJobID *C.char
    var tmpJobID *C.char
    var tmpPriorityName *C.char
    var tmpScheduleType *C.char
    var tmpExecutionTime *C.char
    var tmpTasks *C.char

    tmpMsgID = C.CString(data.MsgID)
    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)
    tmpRecurringJobID = C.CString(data.RecurringJobID)
    tmpJobID = C.CString(data.JobID)
    tmpPriorityName = C.CString(data.PriorityName)
    tmpScheduleType = C.CString(data.ScheduleType)
    tmpExecutionTime = C.CString(data.ExecutionTime)
    tmpTasks = C.CString(string(data.Tasks))

    defer C.free(unsafe.Pointer(tmpMsgID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    defer C.free(unsafe.Pointer(tmpRecurringJobID))
    defer C.free(unsafe.Pointer(tmpJobID))
    defer C.free(unsafe.Pointer(tmpPriorityName))
    defer C.free(unsafe.Pointer(tmpScheduleType))
    defer C.free(unsafe.Pointer(tmpExecutionTime))
    defer C.free(unsafe.Pointer(tmpTasks))

    job.msgIdentifier.msgID = tmpMsgID
    job.msgIdentifier.robotID = tmpRobotID
    job.msgIdentifier.companyID = tmpCompanyID
    job.msgIdentifier.messageTimeStamp = C.longlong(time.Now().Unix())
    job.recurringJobID = tmpRecurringJobID
    job.jobID = tmpJobID
    job.priority = tmpPriorityName
    job.scheduleType = tmpScheduleType
    job.executionTime = tmpExecutionTime
    job.tasks = tmpTasks
    job.jobCreatedDate = C.longlong(data.JobCreatedDate.Unix())
    job.startDate = C.longlong(data.StartDate.Unix())
    job.endDate = C.longlong(data.EndDate.Unix())
    job.recurringDays = C.int(data.RecurringDays)
    if data.Abort == true {
        job.abort = 1
    } else {
        job.abort = 0
    }

    // publish Robot jobs via DDS
    ret := C.PublishJob(job)
    // fmt.Println("after publish job to robot")
    if ret != 0 {
        log.Println("Failed to publish job:", job)
    } else {
        fmt.Println("pubJobToDDS:", msg)
        log.Println("pubJobToDDS:", msg)
    }

}

func SubAccessKeyResponseFromRedis() {

    // open the log file to log the job
    currentDate := time.Now()
    path := fmt.Sprintf("log/info-job-%s.log", currentDate.Format("2006-01-02"))
    f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("error opening file: %v", err)
    }
    defer f.Close()
    log.SetOutput(f)

    // // Subscribe for AccessKeyReply channel
    // redisChannelSub := robotRedisClient.Subscribe("AccessKeyResponseChannel")
    // if redisChannelSub != nil {
    //  fmt.Println("Ready to Received AccessKeyResponse")
    // } else {
    //  fmt.Println("redisChannelSub for Received AccessKeyResponse is nil")
    // }

    // for {
        // // subscribe new job from redis job channel
        // accessKeyResponse, err := redisChannelSub.ReceiveMessage()
        // if err != nil {
        //  log.Println("Received AccessKeyResponse Message Error: ", err)
        // } else {
        //  log.Println("Received AccessKeyResponse Message without Error")
        // }

        // // publish the new job to robot via DDS
        // if (accessKeyResponse != nil) && (accessKeyResponse.Payload != "") {
        //  pubAccessKeyResponseToDDS(accessKeyResponse.Payload)
        // } else {
        //  log.Println("(accessKeyResponse == nil) OR (accessKeyResponse.Payload == ")
        // }
    // }

    /////////////////////////////////////////////////////////////////////////////////////
    pubsub := robotRedisClient.Subscribe("AccessKeyResponseChannel")
    defer pubsub.Close()

    if _, err := pubsub.Receive(); err != nil {
        log.Println("Received AccessKeyResponse Message Error: ", err)
        // return
    }

    keyResponseCh := pubsub.Channel()
    fmt.Println("Ready to Received AccessKeyResponseChannel PubSub")

    // Endlessly listen to control channel,
    for msg := range keyResponseCh {

        // publish the new job to robot via DDS
        if (msg != nil) && (msg.Payload != "") {
            pubAccessKeyResponseToDDS(msg.Payload)
        } else {
            log.Println("(msg == nil) OR (msg.Payload == ")
        }
    }
}

func pubAccessKeyResponseToDDS(msg string) {

    // open the log file to log the job
    currentDate := time.Now()
    path := fmt.Sprintf("log/info-job-%s.log", currentDate.Format("2006-01-02"))
    f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("error opening file: %v", err)
    }
    defer f.Close()
    log.SetOutput(f)

    log.Println("pubAccessKeyResponseToDDS()")

    var responose C.DDS_AccessKey
    data := AccessKey{}
    json.Unmarshal([]byte(msg), &data)

    // convert redis data format to DDS
    var tmpRobotID *C.char
    var tmpCompanyID *C.char
    var tmpAccessKey *C.char
    var tmpAdaptorKey *C.char

    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)
    tmpAccessKey = C.CString(data.AccessKey)
    tmpAdaptorKey = C.CString(data.AdaptorKey)

    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    defer C.free(unsafe.Pointer(tmpAccessKey))
    defer C.free(unsafe.Pointer(tmpAdaptorKey))

    responose.msgIdentifier.robotID = tmpRobotID
    responose.msgIdentifier.companyID = tmpCompanyID
    responose.msgIdentifier.messageTimeStamp = C.longlong(time.Now().Unix())
    responose.accessKey = tmpAccessKey
    responose.adaptorKey = tmpAdaptorKey

    // publish access key reply via DDS
    ret := C.PublishAccessKeyResponse(responose)
    fmt.Println("after publish access key response to robot")
    if ret != 0 {
        fmt.Println("Failed to publish access key response:", msg)
        log.Println("Failed to publish access key response:", msg)
    } else {
        fmt.Println("pubAccessKeyResponseToDDS: ", msg)
        log.Println("pubAccessKeyResponseToDDS:", msg)
    }
}

func SubRobotJobResponseFromRedis() {

    // Subscribe for RobotJobResponse channel
    redisChannelSub := robotRedisClient.Subscribe("RobotJobResponseChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received RobotJobResponse")
    } else {
        fmt.Println("redisChannelSub for Received RobotJobResponse is nil")
    }

    for {
        // subscribe new job from redis RobotJobResponse channel
        jobResponse, _ := redisChannelSub.ReceiveMessage()

        // publish the JobResponse to robot via DDS
        if (jobResponse != nil) && (jobResponse.Payload != "") {
            pubRobotJobResponseToDDS(jobResponse.Payload)
        }
    }
}

func pubRobotJobResponseToDDS(msg string) {

    var responose C.DDS_RobotJobResponse
    data := RobotJobResponse{}
    json.Unmarshal([]byte(msg), &data)

    // convert redis data format to DDS
    var tmpRobotID *C.char
    var tmpCompanyID *C.char
    var tmpJobID *C.char

    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)
    tmpJobID = C.CString(data.JobID)

    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    defer C.free(unsafe.Pointer(tmpJobID))

    responose.msgIdentifier.robotID = tmpRobotID
    responose.msgIdentifier.companyID = tmpCompanyID
    responose.msgIdentifier.messageTimeStamp = C.longlong(time.Now().Unix())
    responose.jobID = tmpJobID
    if data.RequestSuccess {
        responose.requestSuccess = C.uchar(1)
    } else {
        responose.requestSuccess = C.uchar(0)
    }

    // publish job response reply via DDS
    ret := C.PublishRobotJobResponse(responose)
    fmt.Println("after publish robot job response to robot")
    if ret != 0 {
        log.Println("Failed to publish robot job response:", responose)
    } else {
        fmt.Println("pubRobotJobResponseToDDS: ", msg)
    }
}

// notification related
func SubNotificationResponseFromRedis() {

    // Subscribe for JobResponse channel
    redisChannelSub := robotRedisClient.Subscribe("NotificationResponseChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received NotificationResponse")
    } else {
        fmt.Println("redisChannelSub for Received NotificationResponse is nil")
    }

    for {
        // subscribe new notification from redis NotificationResponse channel
        notiResponse, _ := redisChannelSub.ReceiveMessage()

        // publish the NotificationResponse to robot via DDS
        if (notiResponse != nil) && (notiResponse.Payload != "") {
            pubNotificationResponseToDDS(notiResponse.Payload)
        }
    }
}

func pubNotificationResponseToDDS(msg string) {

    var responose C.DDS_NotificationResponse
    data := NotificationResponse{}
    json.Unmarshal([]byte(msg), &data)

    // convert redis data format to DDS
    var tmpRobotID *C.char
    var tmpCompanyID *C.char
    var tmpNotiID *C.char
    var tmpStatus *C.char
    var tmpDetails *C.char

    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)
    tmpNotiID = C.CString(data.NotificationID)
    tmpStatus = C.CString(data.Status)
    tmpDetails = C.CString(data.Details)

    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    defer C.free(unsafe.Pointer(tmpNotiID))
    defer C.free(unsafe.Pointer(tmpStatus))
    defer C.free(unsafe.Pointer(tmpDetails))

    responose.msgIdentifier.robotID = tmpRobotID
    responose.msgIdentifier.companyID = tmpCompanyID
    responose.msgIdentifier.messageTimeStamp = C.longlong(time.Now().Unix())
    responose.notificationID = tmpNotiID
    responose.status = tmpStatus
    responose.details = tmpDetails

    // publish job response reply via DDS
    ret := C.PublishNotificationResponse(responose)
    if ret != 0 {
        log.Println("Failed to publish notification response:", responose)
    } else {
        fmt.Println("pubNotificationResponseToDDS: ", msg)
    }
}

func SubJobRequestResponseFromRedis() {

    // Subscribe for RobotJobResponse channel
    redisChannelSub := robotRedisClient.Subscribe("JobRequestResponseChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received JobRequestResponse")
    } else {
        fmt.Println("redisChannelSub for Received JobRequestResponse is nil")
    }

    for {
        // subscribe new job from redis JobRequestResponse channel
        requestResponse, _ := redisChannelSub.ReceiveMessage()

        // publish the JobRequestResponse to robot via DDS
        if (requestResponse != nil) && (requestResponse.Payload != "") {
            pubJobRequestResponseToDDS(requestResponse.Payload)
        }
    }
}

func pubJobRequestResponseToDDS(msg string) {

    var responose C.DDS_JobRequestResponse
    data := JobRequestResponse{}
    json.Unmarshal([]byte(msg), &data)

    // convert redis data format to DDS
    var tmpRobotID *C.char
    var tmpCompanyID *C.char

    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)

    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))

    responose.msgIdentifier.robotID = tmpRobotID
    responose.msgIdentifier.companyID = tmpCompanyID
    responose.msgIdentifier.messageTimeStamp = C.longlong(time.Now().Unix())
    if data.RequestSuccess {
        responose.requestSuccess = C.uchar(1)
    } else {
        responose.requestSuccess = C.uchar(0)
    }

    // publish job response reply via DDS
    ret := C.PublishJobRequestResponse(responose)
    fmt.Println("after publish job request response to robot")
    if ret != 0 {
        log.Println("Failed to publish job request response:", responose)
    } else {
        fmt.Println("PublishJobRequestResponse: ", msg)
    }
}

func SubJobStatusAckFromRedis() {

    // Subscribe for AccessKeyReply channel
    redisChannelSub := robotRedisClient.Subscribe("JobStatusAckChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received JobStatusAckChannel")
    } else {
        fmt.Println("redisChannelSub for Received JobStatusAckChannel is nil")
    }

    for {
        // subscribe new job from redis job channel
        statusAck, _ := redisChannelSub.ReceiveMessage()

        // publish the new job to robot via DDS
        if (statusAck != nil) && (statusAck.Payload != "") {
            pubJobStatusAckToDDS(statusAck.Payload)
        }
    }
}

func pubJobStatusAckToDDS(msg string) {

    // open the log file to log the job status ack
    currentDate := time.Now()
    path := fmt.Sprintf("log/info-job-%s.log", currentDate.Format("2006-01-02"))
    f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("error opening file: %v", err)
    }
    defer f.Close()
    log.SetOutput(f)

    var responose C.DDS_MessageAck
    data := RobotMsgAck{}
    json.Unmarshal([]byte(msg), &data)

    // convert redis data format to DDS
    var tmpMessageID *C.char
    var tmpRobotID *C.char
    var tmpCompanyID *C.char

    tmpMessageID = C.CString(data.MsgID)
    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)

    defer C.free(unsafe.Pointer(tmpMessageID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    
    responose.msgIdentifier.msgID = tmpMessageID
    responose.msgIdentifier.robotID = tmpRobotID
    responose.msgIdentifier.companyID = tmpCompanyID
    responose.msgIdentifier.messageTimeStamp = C.longlong(time.Now().Unix())

    // publish access key reply via DDS
    ret := C.PublishJobStatusAck(responose)
    fmt.Println("after publish job status ack to robot")
    if ret != 0 {
        log.Println("Failed to publish job status ack:", responose)
    } else {
        fmt.Println("pubJobStatusAckToDDS: ", msg)
        log.Println("pubJobStatusAckToDDS: ", msg)
    }
}

func SubTeleopRequestFromRedis() {

    // Subscribe to the teleoperation request channel
    redisChannelSub := robotRedisClient.Subscribe("TeleopRequestChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received TeleopRequestChannel")
    } else {
        fmt.Println("redisChannelSub for Received TeleopRequestChannel is nil")
    }

    for {
        // Subscribe new teleoperation request from redis teleoperation request channel
        teleopRequest, _ := redisChannelSub.ReceiveMessage()

        // Publish the new teleopRequest to robot via DDS
        if (teleopRequest != nil) && (teleopRequest.Payload != "") {
            pubTeleopRequestToDDS(teleopRequest.Payload)
        }
    }
}

func pubTeleopRequestToDDS(msg string) {

    var teleopRequest C.DDS_TeleoperationRequest
    data := TeleoperationRequest{}
    json.Unmarshal([]byte(msg), &data)

    // Convert MQTT data format to DDS
    var tmpMsgID *C.char
    var tmpRobotID *C.char
    var tmpCompanyID *C.char

    tmpMsgID = C.CString(data.MsgID)
    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)

    defer C.free(unsafe.Pointer(tmpMsgID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))

    teleopRequest.msgIdentifier.msgID = tmpMsgID
    teleopRequest.msgIdentifier.robotID = tmpRobotID
    teleopRequest.msgIdentifier.companyID = tmpCompanyID
    teleopRequest.msgIdentifier.messageTimeStamp = C.longlong(data.TimeStamp)
    if data.TeleopMode {
        teleopRequest.teleopMode = C.uchar(1)
    } else {
        teleopRequest.teleopMode = C.uchar(0)
    }

    // publish teleoperation request via DDS
    ret := C.PublishTeleopRequest(teleopRequest)
    fmt.Println("after publish teleoperation request to robot")
    if ret != 0 {
        log.Println("Failed to publish teleoperation request:", teleopRequest)
    } else {
        fmt.Println("pubTeleopRequestToDDS:", msg)
    }
}

func SubRobotControlFromRedis() {

    // Subscribe to the robot control channel
    redisChannelSub := robotRedisClient.Subscribe("RobotControlChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received RobotControlChannel")
    } else {
        fmt.Println("redisChannelSub for Received RobotControlChannel is nil")
    }

    for {
        // Subscribe new robot control from redis robot control channel
        robotControl, _ := redisChannelSub.ReceiveMessage()

        // Publish the new robotControl to robot via DDS
        if (robotControl != nil) && (robotControl.Payload != "") {
            pubRobotControlToDDS(robotControl.Payload)
        }
    }
}

func pubRobotControlToDDS(msg string) {

    var robotControl C.DDS_RobotControl
    data := RobotControl{}
    json.Unmarshal([]byte(msg), &data)

    // Convert MQTT data format to DDS
    var tmpMsgID *C.char
    var tmpRobotID *C.char
    var tmpCompanyID *C.char

    tmpMsgID = C.CString(data.MsgID)
    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)

    defer C.free(unsafe.Pointer(tmpMsgID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))

    robotControl.msgIdentifier.msgID = tmpMsgID
    robotControl.msgIdentifier.robotID = tmpRobotID
    robotControl.msgIdentifier.companyID = tmpCompanyID
    robotControl.msgIdentifier.messageTimeStamp = C.longlong(data.TimeStamp)
    robotControl.linearVelocity.x = C.double(data.LinearVelocityX)
    robotControl.linearVelocity.y = C.double(data.LinearVelocityY)
    robotControl.linearVelocity.z = C.double(data.LinearVelocityZ)
    robotControl.angularVelocity.x = C.double(data.AngularVelocityX)
    robotControl.angularVelocity.y = C.double(data.AngularVelocityY)
    robotControl.angularVelocity.z = C.double(data.AngularVelocityZ)
    // if data.robotControlMode {
    //  robotControl.teleopMode = C.uchar(1)
    // } else {
    //  robotControl.teleopMode = C.uchar(0)
    // }

    // publish robot control via DDS
    ret := C.PublishRobotControl(robotControl)
    fmt.Println("after publish robot control to robot")
    if ret != 0 {
        log.Println("Failed to publish robot control:", robotControl)
    } else {
        fmt.Println("pubRobotControlToDDS:", msg)
    }
}

func SubCameraControlFromRedis() {

    // Subscribe to the camera control channel
    redisChannelSub := robotRedisClient.Subscribe("CameraControlChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received CameraControlChannel")
    } else {
        fmt.Println("redisChannelSub for Received CameraControlChannel is nil")
    }

    for {
        // Subscribe new camera control from redis camera control channel
        cameraControl, _ := redisChannelSub.ReceiveMessage()

        // Publish the new cameraControl to robot via DDS
        if (cameraControl != nil) && (cameraControl.Payload != "") {
            pubCameraControlToDDS(cameraControl.Payload)
        }
    }
}

func pubCameraControlToDDS(msg string) {

    var cameraControl C.DDS_CameraControl
    data := CameraControl{}
    json.Unmarshal([]byte(msg), &data)

    // Convert MQTT data format to DDS
    var tmpMsgID *C.char
    var tmpRobotID *C.char
    var tmpCompanyID *C.char
    var cameraName *C.char
    var pan *C.char
    var tilt *C.char

    tmpMsgID = C.CString(data.MsgID)
    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)
    cameraName = C.CString(data.CameraName)
    pan = C.CString(data.Pan)
    tilt = C.CString(data.Tilt)

    defer C.free(unsafe.Pointer(tmpMsgID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    defer C.free(unsafe.Pointer(cameraName))
    defer C.free(unsafe.Pointer(pan))
    defer C.free(unsafe.Pointer(tilt))

    cameraControl.msgIdentifier.msgID = tmpMsgID
    cameraControl.msgIdentifier.robotID = tmpRobotID
    cameraControl.msgIdentifier.companyID = tmpCompanyID
    cameraControl.msgIdentifier.messageTimeStamp = C.longlong(data.TimeStamp) 
    cameraControl.cameraName = cameraName
    cameraControl.pan = pan
    cameraControl.tilt = tilt
    cameraControl.zoomMultiplier = C.double(data.ZoomMultiplier)

    // publish camera control via DDS
    ret := C.PublishCameraControl(cameraControl)
    fmt.Println("after publish camera control to robot")
    if ret != 0 {
        log.Println("Failed to publish camera control:", cameraControl)
    } else {
        fmt.Println("pubCameraControlToDDS:", msg)
    }
}

func SubRobotConfigAckFromRedis() {

    // Subscribe for RobotConfigAck channel
    redisChannelSub := robotRedisClient.Subscribe("RobotConfigAckChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received RobotConfigAckChannel")
    } else {
        fmt.Println("redisChannelSub for Received RobotConfigAckChannel is nil")
    }

    for {
        // subscribe robot config acknowledge
        configAck, _ := redisChannelSub.ReceiveMessage()

        // publish the robot config acknowledge to robot via DDS
        if (configAck != nil) && (configAck.Payload != "") {
            pubRobotConfigAckToDDS(configAck.Payload)
        }
    }
}

func pubRobotConfigAckToDDS(msg string) {

    // open the log file to log the job status ack
    currentDate := time.Now()
    path := fmt.Sprintf("log/info-job-%s.log", currentDate.Format("2006-01-02"))
    f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("error opening file: %v", err)
    }
    defer f.Close()
    log.SetOutput(f)

    var response C.DDS_MessageAck
    data := RobotMsgAck{}
    json.Unmarshal([]byte(msg), &data)

    // convert redis data format to DDS
    var tmpMessageID *C.char
    var tmpRobotID *C.char
    var tmpCompanyID *C.char

    tmpMessageID = C.CString(data.MsgID)
    tmpRobotID = C.CString(data.RobotID)
    tmpCompanyID = C.CString(data.CompanyID)

    defer C.free(unsafe.Pointer(tmpMessageID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    
    response.msgIdentifier.msgID = tmpMessageID
    response.msgIdentifier.robotID = tmpRobotID
    response.msgIdentifier.companyID = tmpCompanyID
    response.msgIdentifier.messageTimeStamp = C.longlong(time.Now().Unix())

    // publish robot config acknowledge via DDS
    ret := C.PublishRobotConfigAck(response)
    fmt.Println("after publish robot config ack to robot")
    if ret != 0 {
        log.Println("Failed to publish robot config ack:", response)
    } else {
        fmt.Println("pubRobotConfigAckToDDS:", msg)
        log.Println("pubRobotConfigAckToDDS:", msg)
    }
}

func SubRobotConferenceCallRequestFromRedis() {

    // Subscribe for RFMConferenceCallRequest channel
    redisChannelSub := robotRedisClient.Subscribe("RFMConferenceCallRequestChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received RFMConferenceCallRequestChannel")
    } else {
        fmt.Println("redisChannelSub for Received RFMConferenceCallRequestChannel is nil")
    }

    for {
        // subscribe robot conference call request
        conferenceRequest, _ := redisChannelSub.ReceiveMessage()

        // publish the robot conference call request to robot via DDS
        if (conferenceRequest != nil) && (conferenceRequest.Payload != "") {
            pubRobotConferenceCallRequestToDDS(conferenceRequest.Payload)
        }
    }
}

func pubRobotConferenceCallRequestToDDS(msg string) {

    // open the log file to log the job status ack
    currentDate := time.Now()
    path := fmt.Sprintf("log/info-vc-%s.log", currentDate.Format("2006-01-02"))
    f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
    if err != nil {
        log.Fatalf("error opening file: %v", err)
    }
    defer f.Close()
    log.SetOutput(f)

    var conferenceCallRequest C.DDS_ConferenceCallRequest
    data := ConferenceCallRequest{}
    json.Unmarshal([]byte(msg), &data)

    // Convert MQTT data format to DDS
    var tmpMsgID = C.CString(data.MsgID)
    var tmpRobotID = C.CString(data.RobotID)
    var tmpCompanyID = C.CString(data.CompanyID)
    var tmpID = C.CString(data.RequestID)
    var tmpMode = C.CString(data.Mode)
    var tmpRoom = C.CString(data.Room)
    
    defer C.free(unsafe.Pointer(tmpMsgID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    defer C.free(unsafe.Pointer(tmpID))
    defer C.free(unsafe.Pointer(tmpRoom))
    defer C.free(unsafe.Pointer(tmpMode))

    conferenceCallRequest.msgIdentifier.msgID = tmpMsgID
    conferenceCallRequest.msgIdentifier.robotID = tmpRobotID
    conferenceCallRequest.msgIdentifier.companyID = tmpCompanyID
    conferenceCallRequest.msgIdentifier.messageTimeStamp = C.longlong(data.TimeStamp)
    conferenceCallRequest.ID = tmpID
    conferenceCallRequest.mode = tmpMode
    conferenceCallRequest.options.room = tmpRoom

    if data.VideoOn {
        conferenceCallRequest.options.videoOn = C.uchar(1)
    } else {
        conferenceCallRequest.options.videoOn = C.uchar(0)
    }
    
    if data.AudioMuted {
        conferenceCallRequest.options.audioMuted = C.uchar(1)
    } else {
        conferenceCallRequest.options.audioMuted = C.uchar(0)
    }

    // publish conference call request via DDS
    ret := C.PublishConferenceCallRequest(conferenceCallRequest)
    fmt.Println("after publish conference call request to robot")
    if ret != 0 {
        log.Println("Failed to publish conference call request:", conferenceCallRequest)
    } else {
        fmt.Println("pubConferenceCallRequestToDDS:", msg)
        log.Println("pubConferenceCallRequestToDDS:", msg)
    }
}

func SubRobotConferenceCallUpdatesFromRedis() {

    // Subscribe for RFMConferenceCallUpdates channel
    redisChannelSub := robotRedisClient.Subscribe("RFMConferenceCallUpdatesChannel")
    if redisChannelSub != nil {
        fmt.Println("Ready to Received RFMConferenceCallUpdatesChannel")
    } else {
        fmt.Println("redisChannelSub for Received RFMConferenceCallUpdatesChannel is nil")
    }

    for {
        // subscribe robot conference call updates
        conferenceUpdates, _ := redisChannelSub.ReceiveMessage()

        // publish the robot conference call updates to robot via DDS
        if (conferenceUpdates != nil) && (conferenceUpdates.Payload != "") {
            pubRobotConferenceCallUpdatesToDDS(conferenceUpdates.Payload)
        }
    }
}

func pubRobotConferenceCallUpdatesToDDS(msg string) {

    var conferenceCallUpdates C.DDS_ConferenceCallUpdates
    data := ConferenceCallUpdates{}
    json.Unmarshal([]byte(msg), &data)

    // Convert MQTT data format to DDS
    var tmpMsgID = C.CString(data.MsgID)
    var tmpRobotID = C.CString(data.RobotID)
    var tmpCompanyID = C.CString(data.CompanyID)
    var tmpRequestID = C.CString(data.RequestID)
    var tmpRoom = C.CString(data.Room)
    var tmpStatus = C.CString(data.Status)

    defer C.free(unsafe.Pointer(tmpMsgID))
    defer C.free(unsafe.Pointer(tmpRobotID))
    defer C.free(unsafe.Pointer(tmpCompanyID))
    defer C.free(unsafe.Pointer(tmpRequestID))
    defer C.free(unsafe.Pointer(tmpRoom))
    defer C.free(unsafe.Pointer(tmpStatus))

    conferenceCallUpdates.msgIdentifier.msgID = tmpMsgID
    conferenceCallUpdates.msgIdentifier.robotID = tmpRobotID
    conferenceCallUpdates.msgIdentifier.companyID = tmpCompanyID
    conferenceCallUpdates.msgIdentifier.messageTimeStamp = C.longlong(data.TimeStamp)
    conferenceCallUpdates.requestID = tmpRequestID
    conferenceCallUpdates.options.room = tmpRoom
    conferenceCallUpdates.status = tmpStatus

    if data.VideoOn {
        conferenceCallUpdates.options.videoOn = C.uchar(1)
    } else {
        conferenceCallUpdates.options.videoOn = C.uchar(0)
    }
    
    if data.AudioMuted {
        conferenceCallUpdates.options.audioMuted = C.uchar(1)
    } else {
        conferenceCallUpdates.options.audioMuted = C.uchar(0)
    }

    // publish conference call updates via DDS
    ret := C.PublishConferenceCallUpdates(conferenceCallUpdates)
    fmt.Println("after publish conference call updates to robot")
    if ret != 0 {
        log.Println("Failed to publish conference call updates:", conferenceCallUpdates)
    } else {
        fmt.Println("pubConferenceCallUpdatesToDDS:", msg)
    }
}