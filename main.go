package main

import (
    "encoding/json"
    "fmt"
    "time"
    "godzilla/core/config"
    "log"
    "net/http"
    "os"

    "godzilla/models/schema"
    "godzilla/routes"
    "godzilla/util"
    "godzilla/models/viewmodel"

    "github.com/gorilla/handlers"
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
    redis "github.com/go-redis/redis"

    _lookupRepository "godzilla/modules/lookup/repository"
    _locationRepository "godzilla/modules/location/repository"
    _robotsRepository "godzilla/modules/robots/repository"
    _robotsUcase "godzilla/modules/robots/usecase"
    _jobRepository "godzilla/modules/job/repository"
    _jobUcase "godzilla/modules/job/usecase"
)

//Appserver Entry
func main() {
    //Create Log Folder Path in the application root directory
    if _, err := os.Stat("log"); os.IsNotExist(err) {
        os.Mkdir("log", os.ModePerm)
    }
    if _, err := os.Stat("resources"); os.IsNotExist(err) {
        os.Mkdir("resources", os.ModePerm)
    }
    if _, err := os.Stat("resources/temp"); os.IsNotExist(err) {
        os.Mkdir("resources/temp", os.ModePerm)
    }

    fmt.Println("Starting App server ...")
    //Create DB instance
    db, timeout, redis, err := config.Init()
    if err != nil {
        panic(err)
    }
    fmt.Println("Database Connected ...")
    sqlQuery := "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\" WITH SCHEMA dbo"
    err = db.Raw(sqlQuery).Error
    if err != nil {
        fmt.Println("Extention Creation Failed")
    } else {
        fmt.Println("Extention Created successfully")
    }

    if len(redis) > 0 {
        fmt.Println("Clearing redis DB 0")
        redis[0].FlushDB()
        fmt.Println("Clearing redis DB 1")
        redis[1].FlushDB()
    }

    if config.Get().MigrateRequired == true {
        fmt.Println("Database Migrating ...")
        //Migrate database
        Migrate(db)
        fmt.Println("Database Migrated ...")
    }
    //Create Routes
    router := routes.ConfigRoutes(db, timeout, redis)
    if config.Get().Debug == true {
        db.LogMode(true)
    }


    // start the process to listening on the access key request
    ListenRobotAccessKeyRequestChannel(db)

    // start the process to listening on the task created by robot
    ListenRobotTaskChannel(db, timeout, redis)

    // start the process to listening on the task request from robot
    ListenTaskRequestChannel(db, timeout, redis)

    // start the process to listening on the robot log from robot
    ListenRobotLogChannel(db, timeout, redis)

    fmt.Println("Listening Port no : ", config.Get().ServerPort)
    allowedOrigins := handlers.AllowedOrigins([]string{"*"})
    allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})
    log.Fatal(http.ListenAndServe(config.Get().ServerPort,
        handlers.CORS(allowedOrigins, allowedMethods)(router)))
}

//Migrate Database and Create Tables and PrimaryKey/ForeignKey
func Migrate(db *gorm.DB) {
    db.AutoMigrate(
        &schema.Modules{},
        &schema.ModulesAuthority{},
        &schema.Subscription{},
        &schema.SubscriptionModules{},
        &schema.Company{},
        &schema.CompanySubscription{},
        &schema.Config{},
        &schema.Layout{},
        &schema.LayoutMarker{},
        &schema.LinkArea{},
        &schema.LinkAreaLocation{},
        &schema.LinkAreaMarker{},
        &schema.Location{},
        &schema.Lookup{},
        &schema.LookupMaster{},
        &schema.Map{},
        &schema.MapVersion{},
        &schema.Marker{},
        &schema.RobotModel{},
        &schema.ModelSkillset{},
        &schema.ModelConfiguration{},
        &schema.ModelFirmware{},
        &schema.Robot{},
        &schema.RobotConfiguration{},
        &schema.RobotFirmware{},
        &schema.RobotMap{},
        &schema.Notification{},
        &schema.RobotSkillset{},
        &schema.RobotStatus{},
        &schema.SendToRobot{},
        &schema.Skillset{},
        &schema.Job{},
        &schema.JobActivity{},
        &schema.JobParameter{},
        // &schema.Route{},
        // &schema.RoutePoint{},
        // &schema.RoutePointParameter{},
        &schema.Routine{},
        &schema.RoutineTask{},
        &schema.RoutineTaskParameter{},
        &schema.User{},
        &schema.UserAccessRight{},
        &schema.UserLoginTracking{},
        &schema.UserPasswordHistory{},
        &schema.UserRole{},
        &schema.FileStore{},
        &schema.Media{},
        &schema.RobotLog{},
    )
    db.Model(&schema.SubscriptionModules{}).AddForeignKey("subscription_id", "subscriptions(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.SubscriptionModules{}).AddForeignKey("module_id", "modules(id)", "RESTRICT", "RESTRICT")
    //db.Model(&schema.Company{}).AddForeignKey("company_subscription_id", "company_subscriptions(id)", "RESTRICT", "RESTRICT")
    //db.Model(&schema.CompanySubscription{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    //db.Model(&schema.CompanySubscription{}).AddForeignKey("subscription_id", "subscriptions(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Config{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Layout{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Layout{}).AddForeignKey("location_id", "locations(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.LayoutMarker{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.LayoutMarker{}).AddForeignKey("layout_id", "layouts(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.LayoutMarker{}).AddForeignKey("marker_id", "markers(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.LinkArea{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.LinkAreaLocation{}).AddForeignKey("link_area_id", "link_areas(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.LinkAreaMarker{}).AddForeignKey("link_area_id", "link_areas(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.LinkAreaMarker{}).AddForeignKey("link_area_location_id", "link_area_locations(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Location{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    //db.Model(&schema.Lookup{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Lookup{}).AddForeignKey("lookup_master_id", "lookup_masters(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Map{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Map{}).AddForeignKey("src_model_id", "robot_models(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Map{}).AddForeignKey("src_robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Map{}).AddForeignKey("location_id", "locations(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.MapVersion{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.MapVersion{}).AddForeignKey("map_id", "maps(id)", "CASCADE", "RESTRICT")
    db.Model(&schema.Marker{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModulesAuthority{}).AddForeignKey("module_id", "modules(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotModel{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Robot{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Robot{}).AddForeignKey("robot_model_id", "robot_models(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModelConfiguration{}).AddForeignKey("model_id", "robot_models(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModelConfiguration{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModelFirmware{}).AddForeignKey("model_id", "robot_models(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModelFirmware{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModelSkillset{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModelSkillset{}).AddForeignKey("model_id", "robot_models(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.ModelSkillset{}).AddForeignKey("skillset_id", "skillsets(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotConfiguration{}).AddForeignKey("model_id", "robot_models(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotConfiguration{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotConfiguration{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotFirmware{}).AddForeignKey("model_id", "robot_models(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotFirmware{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotFirmware{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotMap{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotMap{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotMap{}).AddForeignKey("location_id", "locations(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotMap{}).AddForeignKey("map_ver_id", "map_versions(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Skillset{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotSkillset{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotSkillset{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotSkillset{}).AddForeignKey("skillset_id", "skillsets(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.SendToRobot{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.SendToRobot{}).AddForeignKey("map_id", "maps(id)", "CASCADE", "RESTRICT")
    db.Model(&schema.RobotStatus{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotStatus{}).AddForeignKey("task_id", "tasks(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotStatus{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotStatus{}).AddForeignKey("map_id", "maps(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Notification{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Notification{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Notification{}).AddForeignKey("map_ver_id", "map_versions(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Job{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Job{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Job{}).AddForeignKey("skillset_id", "skillsets(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Job{}).AddForeignKey("priority_id", "lookups(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Job{}).AddForeignKey("job_execution_id", "lookups(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.JobParameter{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.JobParameter{}).AddForeignKey("job_id", "jobs(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.JobActivity{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.JobActivity{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.Route{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.Route{}).AddForeignKey("location_id", "locations(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.Route{}).AddForeignKey("layout_id", "layouts(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.RoutePoint{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.RoutePoint{}).AddForeignKey("route_id", "routes(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.RoutePoint{}).AddForeignKey("layout_marker_id", "layout_markers(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.RoutePointParameter{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.RoutePointParameter{}).AddForeignKey("route_point_id", "route_point(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Routine{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Routine{}).AddForeignKey("location_id", "locations(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Routine{}).AddForeignKey("layout_id", "layouts(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RoutineTask{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RoutineTask{}).AddForeignKey("composite_task_id", "routines(id)", "RESTRICT", "RESTRICT")
    // db.Model(&schema.RoutineTask{}).AddForeignKey("layout_marker_id", "layout_markers(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RoutineTaskParameter{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RoutineTaskParameter{}).AddForeignKey("composite_task_activity_id", "routine_tasks(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.User{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.User{}).AddForeignKey("user_role_id", "user_roles(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.UserAccessRight{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.UserAccessRight{}).AddForeignKey("user_id", "users(id)", "CASCADE", "RESTRICT")
    db.Model(&schema.UserLoginTracking{}).AddForeignKey("user_id", "users(id)", "CASCADE", "RESTRICT")
    db.Model(&schema.UserPasswordHistory{}).AddForeignKey("user_id", "users(id)", "CASCADE", "RESTRICT")
    db.Model(&schema.FileStore{}).AddForeignKey("module_id", "modules(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.Media{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotLog{}).AddForeignKey("company_id", "companies(id)", "RESTRICT", "RESTRICT")
    db.Model(&schema.RobotLog{}).AddForeignKey("robot_id", "robots(id)", "RESTRICT", "RESTRICT")

    //Migrate Default Data
    var count int
    var companyID = util.GetUUID("c0000000-c000-c000-c000-c00000000001")
    var companyCommonID = util.GetUUID("c0000000-c000-c000-c000-c00000000000")
    var userID = util.GetUUID("a0000000-a000-a000-a000-a00000000001")

    fmt.Println(companyID, companyCommonID, userID)
    db.Find(&schema.UserRole{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into UserRoles ...")
        userRole := []*schema.UserRole{
            {ID: 1, Name: "Super Admin", Description: "Super Admin"},
            {ID: 2, Name: "Administrator", Description: "Administrator"},
            {ID: 3, Name: "User", Description: "User"},
        }
        for _, item := range userRole {
            db.Create(&item)
        }
    }
    db.Find(&schema.Subscription{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into Subscription ...")
        subscription := []*schema.Subscription{
            {Name: "Basic", Description: "Basic", IsActive: true},
            {Name: "Premium", Description: "Premium", IsActive: true},
            {Name: "Enterprise", Description: "Enterprise", IsActive: true},
        }
        for _, item := range subscription {
            db.Create(&item)
        }
    }
    db.Find(&schema.Modules{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into Modules ...")
        modules := []*schema.Modules{
            {ID: 1, Name: "Dashboard", Description: "Dashboard", Path: "", IsActive: true, ModuleSeq: 1},
            {ID: 2, Name: "Monitor", Description: "Monitor", Path: "", IsActive: true, ModuleSeq: 2},
            {ID: 3, Name: "Task", Description: "Task", Path: "", IsActive: true, ModuleSeq: 3},
            {ID: 4, Name: "Notification", Description: "Notification", Path: "", IsActive: false, ModuleSeq: 4},
            {ID: 5, Name: "Robots", Description: "Robots", Path: "", IsActive: true, ModuleSeq: 5},
            {ID: 6, Name: "Utility Summary", Description: "Utility Summary", Path: "Reports", IsActive: false, ModuleSeq: 6},
            {ID: 7, Name: "Fault Analysis", Description: "Fault Analysis", Path: "Reports", IsActive: false, ModuleSeq: 7},
            {ID: 8, Name: "Historical Path", Description: "Historical Path", Path: "Reports", IsActive: false, ModuleSeq: 8},
            {ID: 9, Name: "Activity Log", Description: "Activity Log", Path: "Reports", IsActive: false, ModuleSeq: 9},
            {ID: 10, Name: "Users", Description: "Users", Path: "Settings", IsActive: true, ModuleSeq: 10},
            {ID: 11, Name: "Robot Details", Description: "Robots", Path: "Settings", IsActive: true, ModuleSeq: 11},
            {ID: 12, Name: "Models", Description: "Models", Path: "Settings/Robots", IsActive: true, ModuleSeq: 12},
            {ID: 13, Name: "Skillsets", Description: "Skillsets", Path: "Settings/Robots", IsActive: true, ModuleSeq: 13},
            {ID: 14, Name: "Locations", Description: "Locations", Path: "Settings", IsActive: true, ModuleSeq: 14},
            {ID: 15, Name: "Maps", Description: "Maps", Path: "Settings/Locations", IsActive: true, ModuleSeq: 15},
            {ID: 16, Name: "Markers", Description: "Markers", Path: "Settings/Locations", IsActive: true, ModuleSeq: 16},
            {ID: 17, Name: "Link Area", Description: "Link Area", Path: "Settings/Locations", IsActive: false, ModuleSeq: 17},
            {ID: 18, Name: "Options", Description: "Options", Path: "Settings", IsActive: true, ModuleSeq: 18},
            {ID: 19, Name: "API Keys", Description: "API Keys", Path: "Settings/Integration", IsActive: false, ModuleSeq: 19},
            {ID: 20, Name: "Vertical Apps", Description: "Vertical Apps", Path: "Settings/Integration", IsActive: false, ModuleSeq: 20},
            {ID: 21, Name: "Lift", Description: "Lift", Path: "Settings", IsActive: true, ModuleSeq: 21},
            {ID: 22, Name: "Lift Dashboard", Description: "Lift Dashboard", Path: "", IsActive: true, ModuleSeq: 22},
            {ID: 23, Name: "Live View", Description: "Live View", Path: "", IsActive: true, ModuleSeq: 23},
            {ID: 24, Name: "Composite Task", Description: "Composite Task", Path: "", IsActive: true, ModuleSeq: 24},
            {ID: 25, Name: "Media", Description: "Media", Path: "", IsActive: true, ModuleSeq: 25},
            {ID: 26, Name: "Robot Log", Description: "Robot Log", Path: "", IsActive: true, ModuleSeq: 26},
        }
        for _, item := range modules {
            db.Create(&item)
        }
    }
    db.Find(&schema.ModulesAuthority{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into ModulesAuthority ...")
        modulesAuthority := []*schema.ModulesAuthority{
            {ID: 1, ModuleID: 1, Authority: "Read", AuthoritySeq: 1},
            {ID: 2, ModuleID: 2, Authority: "Read", AuthoritySeq: 1},
            {ID: 3, ModuleID: 3, Authority: "Read", AuthoritySeq: 1},
            {ID: 4, ModuleID: 3, Authority: "Write", AuthoritySeq: 2},
            {ID: 5, ModuleID: 3, Authority: "Delete", AuthoritySeq: 3},
            {ID: 6, ModuleID: 4, Authority: "Read", AuthoritySeq: 1},
            {ID: 7, ModuleID: 4, Authority: "Write", AuthoritySeq: 2},
            {ID: 8, ModuleID: 5, Authority: "Read", AuthoritySeq: 1},
            //  {ID: 9, ModuleID: 5, Authority: "Write", AuthoritySeq: 2},
            {ID: 10, ModuleID: 6, Authority: "Read", AuthoritySeq: 1},
            {ID: 11, ModuleID: 7, Authority: "Read", AuthoritySeq: 1},
            {ID: 12, ModuleID: 8, Authority: "Read", AuthoritySeq: 1},
            {ID: 13, ModuleID: 9, Authority: "Read", AuthoritySeq: 1},
            {ID: 14, ModuleID: 10, Authority: "Read", AuthoritySeq: 1},
            {ID: 15, ModuleID: 10, Authority: "Write", AuthoritySeq: 2},
            {ID: 16, ModuleID: 10, Authority: "Delete", AuthoritySeq: 3},
            {ID: 17, ModuleID: 11, Authority: "Read", AuthoritySeq: 1},
            {ID: 18, ModuleID: 11, Authority: "Write", AuthoritySeq: 2},
            {ID: 19, ModuleID: 11, Authority: "Delete", AuthoritySeq: 3},
            {ID: 20, ModuleID: 12, Authority: "Read", AuthoritySeq: 1},
            {ID: 21, ModuleID: 12, Authority: "Write", AuthoritySeq: 2},
            {ID: 22, ModuleID: 12, Authority: "Delete", AuthoritySeq: 3},
            {ID: 23, ModuleID: 13, Authority: "Read", AuthoritySeq: 1},
            {ID: 24, ModuleID: 13, Authority: "Write", AuthoritySeq: 2},
            {ID: 25, ModuleID: 13, Authority: "Delete", AuthoritySeq: 3},
            {ID: 26, ModuleID: 14, Authority: "Read", AuthoritySeq: 1},
            {ID: 27, ModuleID: 14, Authority: "Write", AuthoritySeq: 2},
            {ID: 28, ModuleID: 14, Authority: "Delete", AuthoritySeq: 3},
            {ID: 29, ModuleID: 15, Authority: "Read", AuthoritySeq: 1},
            {ID: 30, ModuleID: 15, Authority: "Write", AuthoritySeq: 2},
            {ID: 31, ModuleID: 15, Authority: "Delete", AuthoritySeq: 3},
            {ID: 32, ModuleID: 16, Authority: "Read", AuthoritySeq: 1},
            {ID: 33, ModuleID: 16, Authority: "Write", AuthoritySeq: 2},
            {ID: 34, ModuleID: 16, Authority: "Delete", AuthoritySeq: 3},
            {ID: 35, ModuleID: 17, Authority: "Read", AuthoritySeq: 1},
            {ID: 36, ModuleID: 17, Authority: "Write", AuthoritySeq: 2},
            {ID: 37, ModuleID: 17, Authority: "Delete", AuthoritySeq: 3},
            {ID: 38, ModuleID: 18, Authority: "Read", AuthoritySeq: 1},
            {ID: 39, ModuleID: 18, Authority: "Write", AuthoritySeq: 2},
            {ID: 40, ModuleID: 18, Authority: "Delete", AuthoritySeq: 3},
            {ID: 41, ModuleID: 19, Authority: "Read", AuthoritySeq: 1},
            {ID: 42, ModuleID: 19, Authority: "Write", AuthoritySeq: 2},
            {ID: 43, ModuleID: 19, Authority: "Delete", AuthoritySeq: 3},
            {ID: 44, ModuleID: 20, Authority: "Read", AuthoritySeq: 1},
            {ID: 45, ModuleID: 20, Authority: "Write", AuthoritySeq: 2},
            {ID: 46, ModuleID: 20, Authority: "Delete", AuthoritySeq: 3},
            {ID: 47, ModuleID: 21, Authority: "Read", AuthoritySeq: 1},
            {ID: 48, ModuleID: 21, Authority: "Write", AuthoritySeq: 2},
            {ID: 49, ModuleID: 21, Authority: "Delete", AuthoritySeq: 3},
            {ID: 50, ModuleID: 22, Authority: "Read", AuthoritySeq: 1},
            {ID: 51, ModuleID: 22, Authority: "Write", AuthoritySeq: 2},
            {ID: 52, ModuleID: 23, Authority: "Read", AuthoritySeq: 1},
            {ID: 53, ModuleID: 24, Authority: "Read", AuthoritySeq: 1},
            {ID: 54, ModuleID: 24, Authority: "Write", AuthoritySeq: 2},
            {ID: 55, ModuleID: 24, Authority: "Delete", AuthoritySeq: 3},
            {ID: 56, ModuleID: 25, Authority: "Read", AuthoritySeq: 1},
            {ID: 57, ModuleID: 25, Authority: "Write", AuthoritySeq: 2},
            {ID: 58, ModuleID: 25, Authority: "Delete", AuthoritySeq: 3},
            {ID: 59, ModuleID: 26, Authority: "Read", AuthoritySeq: 1},
        }
        for _, item := range modulesAuthority {
            db.Create(&item)
        }
    }
    db.Find(&schema.SubscriptionModules{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into SubscriptionModules ...")
        subscriptionModules := []*schema.SubscriptionModules{
            {ID: 1, SubscriptionID: 2, ModuleID: 1},
        }
        for _, item := range subscriptionModules {
            db.Create(&item)
        }
    }
    
    db.Find(&schema.Company{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into Company ...")
        company := []*schema.Company{
            {Model: schema.Model{ID:companyID},Name: "Host", Address: "", Email: "", PhoneNo: "", CompanySubscriptionID: 1, IsActive: true},
        }
        for _, item := range company {
            db.Create(&item)
            companyID=item.ID
        }
    }
    db.Find(&schema.Company{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into CompanySubscription ...")
        companySubscription := []*schema.CompanySubscription{
            {CompanyID: companyID, SubscriptionID: 2, IsActive: true, CreatedBy: userID, UpdatedBy: userID},
        }
        for _, item := range companySubscription {
            db.Create(&item)
        }
    }
    db.Find(&schema.User{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into User ...")
        hash, salt, _ := util.HashPassword("Abcd123$")
        user := []*schema.User{
            {Model: schema.Model{ID:userID},CompanyID: companyID, UserName: "admin", DisplayName: "Administrator", Email: "admin@godzilla.com", UserRoleID: 1, PasswordHash: hash, PasswordSalt: salt, IsActive: util.GetTrue(), IsLocked: util.GetFalse(), ForceChangePwd: util.GetFalse(), CreatedBy: userID, UpdatedBy: userID},
        }
        for _, item := range user {
            db.Create(&item)
        }
        fmt.Println("Inserting data into UserPasswordHistory ...")
        userPasswordHistory := []*schema.UserPasswordHistory{
            {UserID: userID, OldPasswordHash: hash, OldPasswordSalt: salt},
        }
        for _, item := range userPasswordHistory {
            db.Create(&item)
        }
    }

    db.Find(&schema.UserAccessRight{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into UserAccessRight ...")
        userAccessRight := []*schema.UserAccessRight{
            {CompanyID: companyID, UserID: userID, AuthorityID: 1, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 2, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 3, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 4, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 5, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 6, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 7, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 8, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 9, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 10, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 11, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 12, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 12, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 13, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 13, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 14, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 14, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 15, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 15, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 16, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 16, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 17, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 17, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 18, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 18, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 19, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 19, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 20, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 20, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 21, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 21, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 22, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 22, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 23, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 23, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 24, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 24, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 25, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 25, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 26, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 26, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 27, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 27, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 28, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 29, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 30, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 31, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 32, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 33, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 34, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 35, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 36, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 37, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 38, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 39, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 40, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 41, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 42, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 43, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 44, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 45, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 46, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 47, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 48, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 49, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 50, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 51, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 52, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 53, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 54, CreatedBy: userID},
            {CompanyID: companyID, UserID: userID, AuthorityID: 55, CreatedBy: userID},
        }
        for _, item := range userAccessRight {
            db.Create(&item)
        }
    }
    db.Find(&schema.LookupMaster{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into LookupMaster ...")
        lookupMaster := []*schema.LookupMaster{
            {ID: 1, Name: "Priority", Description: "Priority", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
            {ID: 2, Name: "Robot Status", Description: "Robot Status", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
            {ID: 3, Name: "Schedule Type", Description: "Schedule Type", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
            {ID: 4, Name: "Marker Type", Description: "Marker Type", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
            {ID: 5, Name: "Map Format", Description: "Map Format", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
            // {ID: 6, Name: "Task Type", Description: "Task Type (SkillSet Type)", IsActive: util.GetTrue(), ParentID: 0, Reserved: false},
            {ID: 7, Name: "Brand", Description: "Brand", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
            {ID: 8, Name: "Lift Site", Description: "Lift Site", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
            {ID: 9, Name: "Lift Type", Description: "Lift Type", IsActive: util.GetTrue(), ParentID: 0, Reserved: true},
        }
        for _, item := range lookupMaster {
            db.Create(&item)
        }
    }
    db.Find(&schema.Lookup{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into Lookup ...")
        lookup := []*schema.Lookup{
            //Marker Type
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000001")},CompanyID: companyCommonID, LookupMasterID: 4, Name: "Marker", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000002")},CompanyID: companyCommonID, LookupMasterID: 4, Name: "Zone", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000003")},CompanyID: companyCommonID, LookupMasterID: 4, Name: "Obstacle", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},

            //Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000001")}/Priority
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000004")},CompanyID: companyCommonID, LookupMasterID: 1, Name: "High", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000005")},CompanyID: companyCommonID, LookupMasterID: 1, Name: "Medium", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000006")},CompanyID: companyCommonID, LookupMasterID: 1, Name: "Low", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000001")}/Robot StatcompanyCommonIDs
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000007")},CompanyID: companyCommonID, LookupMasterID: 2, Name: "Job In-Progress", Data: "Job In-Progress", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000008")},CompanyID: companyCommonID, LookupMasterID: 2, Name: "In-Rest", Data: "In-Rest", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000009")},CompanyID: companyCommonID, LookupMasterID: 2, Name: "Malfunctioning", Data: "Malfunctioning", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000010")},CompanyID: companyCommonID, LookupMasterID: 2, Name: "Charging", Data: "Charging", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000001")}/Schedule TcompanyCommonIDpe
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000011")},CompanyID: companyCommonID, LookupMasterID: 3, Name: "To execute as soon as assigned tasks are completed", Data: "TaskCompleted", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000012")},CompanyID: companyCommonID, LookupMasterID: 3, Name: "To execute on", Data: "Schedule", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000013")},CompanyID: companyCommonID, LookupMasterID: 3, Name: "Abort current task and execute this task", Data: "Abort", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000001")}/Map FormatcompanyCommonID
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000014")},CompanyID: companyCommonID, LookupMasterID: 5, Name: "JPEG", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000015")},CompanyID: companyCommonID, LookupMasterID: 5, Name: "PNG", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //{CompanyID: 0, LookupMasterID: 5, Name: "PGM", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //SkillSet Type
            // {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000016")},CompanyID: companyID, LookupMasterID: 6, Name: "Concierge", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            // {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000017")},CompanyID: companyID, LookupMasterID: 6, Name: "Tour Guide", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //Brand
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000018")},CompanyID: companyID, LookupMasterID: 7, Name: "NCS", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000019")},CompanyID: companyID, LookupMasterID: 7, Name: "UBTech", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //Lift Site
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000020")},CompanyID: companyID, LookupMasterID: 8, Name: "NCS", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000021")},CompanyID: companyID, LookupMasterID: 8, Name: "SIT", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            //Lift Type
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000022")},CompanyID: companyID, LookupMasterID: 9, Name: "KONE", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {Model: schema.Model{ID : util.GetUUID("b0000000-b000-b000-b000-b00000000023")},CompanyID: companyID, LookupMasterID: 9, Name: "OTIS", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},

        }
        for _, item := range lookup {
            db.Create(&item)
        }
    }

    db.Find(&schema.Media{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into Media ...")
        media := []*schema.Media{
            {Type: "Audio", Name: "Surveillance Audio", Description: "For API testing", FileName: "SurveillanceAudio.mp3", FilePath: "/home/kaeyan/godzilla/media/C" + companyID.String() + "/audio/SurveillanceAudio.mp3", FileStatus: "Successful", CompanyID: companyID, CreatedBy: userID, UpdatedBy: userID},
            {Type: "Video", Name: "CodeRed Video", Description: "For API testing", FileName: "CodeRedVideo.mp4", FilePath: "/home/kaeyan/godzilla/media/C" + companyID.String() + "/video/CodeRedVideo.mp4", FileStatus: "Failed", CompanyID: companyID, CreatedBy: userID, UpdatedBy: userID},
        }
        for _, item := range media {
            db.Create(&item)
        }
    }

    db.Find(&schema.Marker{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into Marker ...")
        marker := []*schema.Marker{
            {CompanyID: companyID, Name: "Zone", Description: "Zone", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000002"), Icon: "../../../../assets/img/Layout-Editor/merge.svg", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Obstacle", Description: "Obstacle", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000003"), Icon: "../../../../assets/img/Layout-Editor/Line.svg", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Charging Area", Description: "Charging Area", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/ChargingArea.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Destination", Description: "Destination", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/destination.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Destination Round", Description: "Destination Round", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/RestArea.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Lift Lobby", Description: "Lift Lobby", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/LiftLobby.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Office Door", Description: "Office Door", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/office_door.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Pantry", Description: "Pantry", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/Pantry.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Reception", Description: "Reception", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/Reception.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Rest Area", Description: "Rest Area", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/RestArea.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Toilet", Description: "Toilet", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/toilet.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Male Toilet", Description: "Male Toilet", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/male_toilet.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Female Toilet", Description: "Female Toilet", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/female_toilet.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Board Room", Description: "Board Room", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/board_room.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Hotel Lobby", Description: "Hotel Lobby", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/HotelLobby.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Lounge Area", Description: "Lounge Area", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/LoungeArea.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Painting", Description: "Painting", Type: util.GetUUID("b0000000-b000-b000-b000-b00000000001"), Icon: "../../../../assets/img/MapIcons/markers/Painting.png", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},

        }
        for _, item := range marker {
            db.Create(&item)
        }
    }
    db.Find(&schema.RobotModel{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into RobotModel ...")
        robotModel := []*schema.RobotModel{
            {CompanyID: companyID, RobotBrandID: util.GetUUID("b0000000-b000-b000-b000-b00000000018"), Name: "RNR-01", MapFormat: util.GetUUID("b0000000-b000-b000-b000-b00000000014"), CurrentFirmwareVer: "1.2", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, RobotBrandID: util.GetUUID("b0000000-b000-b000-b000-b00000000018"), Name: "RNR-02", MapFormat: util.GetUUID("b0000000-b000-b000-b000-b00000000014"), CurrentFirmwareVer: "2.0", IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
        }
        for _, item := range robotModel {
            db.Create(&item)
        }
    }
    db.Find(&schema.Skillset{}).Count(&count)
    if count == 0 {
        fmt.Println("Inserting data into Skillset ...")
        skillset := []*schema.Skillset{
            {CompanyID: companyID, Name: "Surveillance", Description: "Surveillance the assigned area", SkillsetTypeID: util.GetUUID("b0000000-b000-b000-b000-b00000000016"), IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Cleaning", Description: "Floor cleaning the assigned area", SkillsetTypeID: util.GetUUID("b0000000-b000-b000-b000-b00000000016"), IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Concierge", Description: "Concierge robot for interacting with people in the assigned area", SkillsetTypeID: util.GetUUID("b0000000-b000-b000-b000-b00000000017"), IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
            {CompanyID: companyID, Name: "Delivery", Description: "Food and drinks delivery in the assigned area", SkillsetTypeID: util.GetUUID("b0000000-b000-b000-b000-b00000000017"), IsActive: util.GetTrue(), CreatedBy: userID, UpdatedBy: userID},
        }
        for _, item := range skillset {
            db.Create(&item)
        }
    }
    /*
    db.Where("key=? and value>=?", "db.version", "v01").Find(&schema.Config{}).Count(&count)
    if count == 0 {

        fmt.Println("Updating data into Lookup ...")
        db.Model(&schema.Lookup{}).Where("id=11").Update("name", "To execute as soon as assigned tasks are completed")
        db.Model(&schema.Lookup{}).Where("id=11").Update("data", "TaskCompleted")
        db.Model(&schema.Lookup{}).Where("id=12").Update("data", "Schedule")
        db.Model(&schema.Lookup{}).Where("id=13").Update("data", "Abort")

        fmt.Println("Inserting data into Config db.version=v01 ...")
        config := []*schema.Config{
            {CompanyID: companyID, Key: "db.version", Value: "v01", CreatedBy: userID, UpdatedBy: 1}}
        for _, item := range config {
            db.Create(&item)
        }

    }
    db.Where("key=? and value>=?", "db.version", "v02").Find(&schema.Config{}).Count(&count)
    if count == 0 {

        fmt.Println("Inserting data into Marker ...")
        marker := []*schema.Marker{
            {CompanyID: companyID, Name: "Charging Area", Description: "Charging Area", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/ChargingArea.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Destination", Description: "Destination", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/destination.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Destination Round", Description: "Destination Round", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/RestArea.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Lift Lobby", Description: "Lift Lobby", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/LiftLobby.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Office Door", Description: "Office Door", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/office_door.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Pantry", Description: "Pantry", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/Pantry.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Reception", Description: "Reception", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/Reception.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Rest Area", Description: "Rest Area", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/RestArea.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Toilet", Description: "Toilet", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/toilet.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Male Toilet", Description: "Male Toilet", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/male_toilet.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Female Toilet", Description: "Female Toilet", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/female_toilet.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Board Room", Description: "Board Room", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/board_room.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Hotel Lobby", Description: "Hotel Lobby", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/HotelLobby.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Lounge Area", Description: "Lounge Area", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/LoungeArea.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
            {CompanyID: companyID, Name: "Painting", Description: "Painting", Type: 1, Icon: "../../../../assets/img/MapIcons/markers/Painting.png", IsActive: util.GetTrue(), CreatedBy: 1, UpdatedBy: 1},
        }
        for _, item := range marker {
            db.Create(&item)
        }

        fmt.Println("Update data into Config db.version=v02 ...")
        db.Model(&schema.Config{}).Where("key=? and company_id=?", "db.version", 1).Update("value", "v02")

    }
    */
    fmt.Println("Insertion Completed")

}

// start the Robot Adapter Access Key Vefication
func ListenRobotAccessKeyRequestChannel(db *gorm.DB) {
    redisClient := config.GetRedisClients()
    go func() {
        if len(redisClient) > 0 {
            //Subscribe TaskStatusChannel redis channel
            redisKeyRequestSub := redisClient[0].Subscribe("AccessKeyRequestChannel")
            for {
                // Receiving task status data from TaskStatusChannel subscription
                accessKeyRequest, _ := redisKeyRequestSub.ReceiveMessage()
                if accessKeyRequest != nil {
                    go func() {
                        fmt.Println(accessKeyRequest)

                        keyRequest := viewmodel.VMRobotAccessKey{}
                        json.Unmarshal([]byte(accessKeyRequest.Payload), &keyRequest)

                        // get robot companyID from database
                        cid := GetRobotByID(db, keyRequest.AccessKey)
                        if !util.IsNilUUIDString(cid) {
                            fmt.Println("Find robot in database: ", keyRequest.AccessKey)

                            keyResponse := viewmodel.VMRobotAccessKey{}
                            keyResponse.RobotID = keyRequest.AccessKey
                            keyResponse.CompanyID = cid
                            keyResponse.AccessKey = keyRequest.AccessKey
                            keyResponse.AdaptorKey = keyRequest.AdaptorKey

                            response, _ := json.Marshal(keyResponse)
                            redisClient[0].Publish("AccessKeyResponseChannel", response)

                        } else {
                            fmt.Println("Cannot find robot in database: ", keyRequest.AccessKey)
                        }
                    }()
                }
            }
        }
    }()
}


//GetRobot returns robot details based on robotID
func GetRobotByID(db *gorm.DB, id string) string {
    companyID := "00000000-0000-0000-0000-000000000000"
    mrobot := schema.Robot{}
    err := db.Select("company_id").
              Find(&mrobot, "id = ?", id).Error

    if err != nil {
        return companyID
    } 

    companyID = util.GetUUIDString(mrobot.CompanyID)
    return companyID
}

// start the listener of the task created by robot
func ListenRobotTaskChannel(db *gorm.DB, timeout time.Duration, redis []*redis.Client) {

    lookupRepo := _lookupRepository.NewLookupRepository(db)
    locationRepo := _locationRepository.NewLocationRepository(db)
    robotsRepo := _robotsRepository.NewRobotsRepository(db)
    jobRepo := _jobRepository.NewJobRepository(db)
    jobUsecase := _jobUcase.NewJobUsecase(jobRepo, robotsRepo, lookupRepo, locationRepo, timeout, redis)

    // get redis client
    redisClient := config.GetRedisClients()
    go func() {
        if len(redisClient) > 0 {
            // Subscribe TaskStatusChannel redis channel
            robotTaskSub := redisClient[0].Subscribe("RobotJobChannel")
            for {
                // Receiving task status data from TaskStatusChannel subscription
                robotTask, _ := robotTaskSub.ReceiveMessage()
                if robotTask != nil {
                    go func() {
                        fmt.Println(robotTask)

                        // define response virable to send to robot 
                        response := viewmodel.VMRobotJobResponse{}
                        response.RequestSuccess = false

                        // unmarshal the task request
                        reqJob := viewmodel.VMRobotJob{}
                        err := json.Unmarshal([]byte(robotTask.Payload), &reqJob)
                        if err == nil {
                            // add the job request
                            err = jobUsecase.AddJobFromRobot(reqJob)
                            if err == nil {
                                response.RequestSuccess = true
                            } else {
                                fmt.Println("Error when AddJobFromRobot: ", err)
                            }
                        } else {
                            fmt.Println("Error when unmarshal task request: ", reqJob.Tasks)
                        }

                        // send response to robot 
                        response.RobotID = reqJob.RobotID
                        response.CompanyID = reqJob.CompanyID
                        response.JobID = reqJob.JobID

                        jsonResponse, _ := json.Marshal(response)
                        redisClient[0].Publish("RobotJobResponseChannel", jsonResponse)
                    }()
                }
            }
        }
    }()
}

// start the listener of the task request from robot
func ListenTaskRequestChannel(db *gorm.DB, timeout time.Duration, redis []*redis.Client) {

    lookupRepo := _lookupRepository.NewLookupRepository(db)
    locationRepo := _locationRepository.NewLocationRepository(db)
    robotsRepo := _robotsRepository.NewRobotsRepository(db)
    jobRepo := _jobRepository.NewJobRepository(db)
    jobUsecase := _jobUcase.NewJobUsecase(jobRepo, robotsRepo, lookupRepo, locationRepo, timeout, redis)

    // get redis client
    redisClient := config.GetRedisClients()
    go func() {
        if len(redisClient) > 0 {
            // Subscribe TaskStatusChannel redis channel
            taskRequestSub := redisClient[0].Subscribe("JobRequestChannel")
            for {
                // Receiving task status data from TaskStatusChannel subscription
                taskRequest, _ := taskRequestSub.ReceiveMessage()
                if taskRequest != nil {
                    go func() {
                        fmt.Println(taskRequest)

                        // define response virable to send to robot 
                        response := viewmodel.VMJobRequestResponse{}
                        response.RequestSuccess = true

                        // unmarshal the task request
                        reqTask := viewmodel.VMJobRequest{}
                        err := json.Unmarshal([]byte(taskRequest.Payload), &reqTask)
                        if err == nil {
                            // add the task request
                            err = jobUsecase.RequestJobFromRobot(reqTask)
                            if err != nil {
                                fmt.Println("Error when Task:jobUsecase:RequestJobFromRobot: ", err)
                            }
                        } else {
                            fmt.Println("Error when unmarshal task request: ")
                        }

                        // send response to robot 
                        response.RobotID = reqTask.RobotID
                        response.CompanyID = reqTask.CompanyID

                        jsonResponse, _ := json.Marshal(response)
                        redisClient[0].Publish("JobRequestResponseChannel", jsonResponse)
                    }()
                }
            }
        }
    }()
}

// start the listener of the robot log from robot
func ListenRobotLogChannel(db *gorm.DB, timeout time.Duration, redis []*redis.Client) {

    // lookupRepo := _lookupRepository.NewLookupRepository(db)
    // locationRepo := _locationRepository.NewLocationRepository(db)
    // robotsRepo := _robotsRepository.NewRobotsRepository(db)
    // jobRepo := _jobRepository.NewJobRepository(db)
    // jobUsecase := _jobUcase.NewJobUsecase(jobRepo, robotsRepo, lookupRepo, locationRepo, timeout, redis)

    robotsRepo := _robotsRepository.NewRobotsRepository(db)
    robotsUsecase := _robotsUcase.NewRobotsUsecase(robotsRepo, timeout)

    // get redis client
    redisClient := config.GetRedisClients()
    go func() {
        if len(redisClient) > 0 {
            // Subscribe TaskStatusChannel redis channel
            taskRequestSub := redisClient[0].Subscribe("RobotLogChannel")
            for {
                // Receiving task status data from TaskStatusChannel subscription
                data, _ := taskRequestSub.ReceiveMessage()
                if data != nil {
                    go func() {
                        fmt.Println(data)

                        // // define response virable to send to robot 
                        // rLog := viewmodel.VMRobotLogItem{}
                        // response.RequestSuccess = true

                        // unmarshal the task request
                        rLog := viewmodel.VMRobotLogItem{}
                        err := json.Unmarshal([]byte(data.Payload), &rLog)
                        if err == nil {
                            // add the robot log
                            err = robotsUsecase.AddRobotLog(rLog)
                            if err != nil {
                                fmt.Println("Error when Robot:robotsUsecase:AddRobotLog: ", err)
                            }
                        } else {
                            fmt.Println("Error when unmarshal robot log")
                        }
                    }()
                }
            }
        }
    }()
}



